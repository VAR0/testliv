//
//  ViewController.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//
import UIKit

protocol SearchDisplayLogic: class
{
    func displayProductInfo(viewModel: [Search.Info.ViewModel])
    func displayImage(imageModel: Search.Img.ImgViewModel)
}

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate
{
    
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var TableResult: UITableView!
    
    
    
    var interactor: SearchBusinessLogic?
    var router: (NSObjectProtocol & SearchRoutingLogic & SearchDataPassing)?
    
    var Products = [Search.Info.ViewModel]()
    var imgProducts = [String:Search.Img.ImgViewModel]()
    var search = ""
    var paged = 1
    var history = [""]
    let defaults = UserDefaults.standard

  // MARK: Object lifecycle
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Setup
  
  private func setup()
  {
    let viewController = self
    let interactor = SearchInteractor()
    let presenter = SearchPresenter()
    let router = SearchRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
   /* if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }*/
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    self.history = defaults.object(forKey: "history") as? [String] ?? [String]()
    TableResult.delegate = self
    TableResult.dataSource = self
    SearchBar.delegate = self
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.imgProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCellSearch
        let product = self.Products[indexPath.row]
        cell.TitleProduct.text = product.name
        cell.Price.text = product.price
        cell.Market.text = product.market
        cell.ImgProduct.image = UIImage(data: self.imgProducts[product.imgProduct]?.imgProduct ?? Data.init())
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.imgProducts.count-1 {
            self.paged+=1
            interactor?.getSearchInfo(request: Search.Info.Request.init(name: self.search, page: String(self.paged)))
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         200
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.search = searchBar.text ?? ""
        self.paged = 1
        self.history.append(self.search)
        self.defaults.set(history, forKey: "history")
        searchBar.endEditing(true)
        
        interactor?.getSearchInfo(request: Search.Info.Request.init(name: self.search , page: String(self.paged)))
        
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.history.contains(searchBar.text ?? "")
        
    }
}

private extension SearchViewController {
    
    
}

extension SearchViewController: SearchDisplayLogic{
    func displayProductInfo(viewModel: [Search.Info.ViewModel]) {
    
        for product in viewModel {
            self.Products.append(product)
            self.imgProducts[product.imgProduct] = Search.Img.ImgViewModel.init(imgName: product.imgProduct, imgProduct: Data.init())
            interactor?.getImageLoad(request: Search.Img.Request.init(img: product.imgProduct))
            
        }
        
        self.TableResult.reloadData()
    }
    
    func displayImage(imageModel: Search.Img.ImgViewModel) {
        self.imgProducts[imageModel.imgName] = imageModel
        self.TableResult.reloadData()
    }
}

class TableViewCellSearch: UITableViewCell{
    
    @IBOutlet weak var TitleProduct: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var Market: UILabel!
    @IBOutlet weak var ImgProduct: UIImageView!
}


