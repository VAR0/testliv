//
//  SearchModels.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//
import UIKit

enum Search
{
  // MARK: Use cases
    
    enum Info
    {
      struct Request
      {
        let name : String
        let page    : String
      }
      struct Product
      {
        var productDisplayName = ""
        var listPrice = 0.0
        var marketplace = ""
        var image = ""
      }
        
      struct ViewModel
      {
        let name: String
        let price: String
        let market: String
        let imgProduct: String
      }
    }
    enum Img
    {
        struct Request {
            let img  : String
        }
        struct ImageResponse {
            let imageProductData : Data
        }
        struct ImgViewModel {
            let imgName : String
            let imgProduct : Data
        }
    }
}


