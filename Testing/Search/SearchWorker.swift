//
//  SearchWorker.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//

import UIKit
import Foundation

class SearchWorker
{
    
    typealias requestInfoResult = (Result<[Search.Info.Product], NSError>) ->()
    func requestProductInfo(name:String, page:String, completion:@escaping requestInfoResult) {
       var url = URLComponents(string: "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/plp/")!

       url.queryItems = [
           URLQueryItem(name: "force-plp", value: "true"),
           URLQueryItem(name: "search-string", value: name),
           URLQueryItem(name: "page-number", value: page),
           URLQueryItem(name: "number-of-items-per-page", value: "10")
       ]
        let request = URLRequest(url: url.url!)

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,                            // is there data
                let response = response as? HTTPURLResponse,  // is there HTTP response
                (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                error == nil else {
                   DispatchQueue.main.async {
                                    completion(.failure(error! as NSError))
                               }// was there no error, otherwise ...
                    
                    return
            }
            let parseResult = self.parseData(data:data)
            
            guard let parsingError = parseResult.1 else {
                DispatchQueue.main.async {
                                completion(.success(parseResult.0))
                           }
                return
            }
            completion(.failure(parsingError))
        
        }
        task.resume()
    }
    typealias requestImageResult = (Result<Search.Img.ImageResponse, NSError>) ->()
    func requestDownloadImage(image: String, completion:@escaping requestImageResult) {
        let url = URLComponents(string: image)!

         let request = URLRequest(url: url.url!)

         let task = URLSession.shared.dataTask(with: request) { data, response, error in
             guard let data = data,                            // is there data
                 let response = response as? HTTPURLResponse,  // is there HTTP response
                 (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                 error == nil else {                           // was there no error, otherwise ...
                    DispatchQueue.main.async {
                     completion(.failure(error! as NSError))
                    }
                     return
             }
            print(data)
            DispatchQueue.main.async {
            completion(.success(Search.Img.ImageResponse.init(imageProductData: data)))
            }
         }
         task.resume()
    }
}

private extension SearchWorker {
    
    func parseData(data dataResponse: Data?) ->([Search.Info.Product], NSError?) {
        
        guard let data = dataResponse else {
            let error = NSError.init(domain: "LoginWorker", code:0, userInfo:[NSLocalizedDescriptionKey: "Error trying to parse response data"])
             return([Search.Info.Product()], error)
        }
        
        do {
            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            var respone = [Search.Info.Product]()
            let respone2 = jsonResponse["plpResults"] as? [String:AnyObject]
            let respone3 = respone2?["records"]
            for product in respone3 as! [AnyObject] {
                respone.append(
                    Search.Info.Product.init(
                        productDisplayName: product["productDisplayName"] as? String ?? "",
                        listPrice: product["listPrice"] as? Double ?? 0.0,
                        marketplace: product["marketplaceBTMessage"] as? String ?? "",
                        image: product["smImage"] as? String ?? "" ))
            }
            return(respone, nil)
           
        } catch let error as NSError {
           return([Search.Info.Product()], error)
        }
    }
    
    
    
    
}
