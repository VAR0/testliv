//
//  SearchInteractor.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//

import UIKit

protocol SearchBusinessLogic
{
    func getSearchInfo(request: Search.Info.Request)
    func getImageLoad(request: Search.Img.Request)
}

protocol SearchDataStore
{
  //var name: String { get set }
}

class SearchInteractor: SearchBusinessLogic, SearchDataStore
{
  var presenter: SearchPresentationLogic?
  var worker: SearchWorker?
  
    
    func getSearchInfo(request: Search.Info.Request) {
        worker = SearchWorker()
        worker?.requestProductInfo(name: request.name, page: request.page, completion: { result in
            switch result {
            case .success(let resp):
                self.presenter?.presentSearchInfo(response: resp)
            case .failure(let error):
                print(error)
                
            }
        })
    }
    
    func getImageLoad(request: Search.Img.Request){
        worker = SearchWorker()
        worker?.requestDownloadImage(image: request.img,completion: { result in
            switch result {
            case .success(let imgresp):
                self.presenter?.downloadImgProduct(name: request.img, response: imgresp)
            case .failure(let error):
                print(error)
            }
            
        })
    }

    
}
