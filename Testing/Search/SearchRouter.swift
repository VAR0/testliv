//
//  SearchRouter.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//

import UIKit

@objc protocol SearchRoutingLogic
{
  //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol SearchDataPassing
{
  var dataStore: SearchDataStore? { get }
}

class SearchRouter: NSObject, SearchRoutingLogic, SearchDataPassing
{
  weak var viewController: SearchViewController?
  var dataStore: SearchDataStore?
  
}
