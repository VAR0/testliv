//
//  SearchPresenter.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//

import UIKit

protocol SearchPresentationLogic
{
    func presentSearchInfo(response: [Search.Info.Product])
    func downloadImgProduct(name: String, response: Search.Img.ImageResponse)
}

class SearchPresenter: SearchPresentationLogic
{
  weak var viewController: SearchDisplayLogic?
  
  // MARK: Do something
    
    func presentSearchInfo(response: [Search.Info.Product]) {
        var viewModel = [Search.Info.ViewModel]()
        for product in response {
            viewModel.append(Search.Info.ViewModel.init(name: product.productDisplayName, price: String(product.listPrice), market: product.marketplace, imgProduct: product.image))
        }
        viewController?.displayProductInfo(viewModel: viewModel)
    }
    
    func downloadImgProduct(name: String, response: Search.Img.ImageResponse){
        let imageModel = Search.Img.ImgViewModel(imgName:name, imgProduct: response.imageProductData)
        viewController?.displayImage(imageModel: imageModel)
    }
}
