//
//  AppDelegate.swift
//  Testing
//
//  Created by Victor Luciano on 04/07/2020.
//  Copyright © 2020 Victor Luciano. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //let loginVC:WelcomeViewController = storyboard.instantiateViewController(withIdentifier: "WelcomeViewControllerId") as! WelcomeViewController
        let search:SearchViewController =  storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let nav = UINavigationController()
        
     /*   if DataSource.getTokenIsValid() {
           let initVC:InitTabBarController = storyboard.instantiateViewController(withIdentifier: "InitTabBarControllerId") as! InitTabBarController
           //let nvc:UINavigationController = storyboar d.instantiateViewControllerWithIdentifier("RootNavigationController") as! UINavigationController
           nav.viewControllers = [loginVC, initVC]
        } else {
            nav.viewControllers = [loginVC]
        }*/
       // nav.viewControllers = [search]
        
        
        /*
        UIView.transition(with: self.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            //self.window?.rootViewController = nav
            UIApplication.shared.windows.first?.rootViewController = nav
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }, completion: nil)
        */
        
        UIApplication.shared.windows.first?.rootViewController = search
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
        return true
    }

    var orientationLock: UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return orientationLock
    }


}

